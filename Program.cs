﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace protectedApiClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //Task.Run(test);
            await test();
        }

        public async static Task test()
        {
            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync("https://is4test.herokuapp.com");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return;
            }

            Console.Write("Enter username please: ");
            var username = Console.ReadLine();

            Console.Write("Enter password please: ");
            var password = Console.ReadLine();

            // request token
            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                UserName = username,
                Password = password,
                ClientId = "client",
                ClientSecret = "highlySecureSecret",
                Scope = "authApi"
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            // call api
            var client1 = new HttpClient();
            client1.SetBearerToken(tokenResponse.AccessToken);

            var response = await client1.GetAsync("https://protectedtestapi.herokuapp.com/identity");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
            }
        }
    }
}
